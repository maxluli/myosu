<?php
session_start();
require_once("php/fonctions.php");
//clef de l'api A NE PAS DIFFUSER
$k = "68ae0b4bb46af407940fcb62f732b2f14b39afc6";
//recuperation du username depuis le formulaire
$username = filter_input(INPUT_POST,"username");
//recuperation du mode de jeu depuis le formulaire
$mode = filter_input(INPUT_POST,"mode");
//si les variables ne sont pas set alors on les set en chaine vide
if(isset($_GET["username"])){
    $username = $_GET["username"];
}
else{
    if(!isset($username)){
        $username = "";
    }
    if(!isset($mode)){
        $mode = "";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="uiKit/css/uikit.min.css" />
    <script src="uiKit/js/uikit.min.js"></script>
    <script src="uiKit/js/uikit-icons.min.js"></script>
    <title>Document</title>
</head>
<body>
    <?php 
    if($username != ""){
        //si la page a bien recu un utilisateur pour en retourner les infos on execute le reste du code
        $u = $username;
        $m = $mode;
        $array = GetUser($username,$mode); 
        displayPP($username,$mode);
        echo $username;
        echo "</br>ici depuis :";
        displayInfoUserJoinDate($username,$mode);
        echo "</br>temps de jeu en heures : ";
        displayInfoUserPlayTimeHours($username,$mode);
        echo "</br>temps de jeu en jours : ";
        displayInfoUserPlayTimeDays($username,$mode);
        echo "</br>rank mondial : ";
        displayInfoUserRank($username,$mode);
        echo "</br>nombre total de play :";
        displayInfoUserPlayCount($username,$mode);
        echo "</br>level : ";
        displayInfoUserLevel($username,$mode);
        echo "</br>pepos : ";
        displayInfoUserPepos($username,$mode);
        echo "</br>accuracy : ";
        displayInfoUserAccu($username,$mode);
        echo "</br>pays : ";
        displayInfoUserPays($username,$mode);
        echo "</br>classement dans le pays :";
        displayInfoUserCountryRank($username,$mode);
        echo "</br>SS+ : ";
        displayInfoUserRankSSH($username,$mode);
        echo "</br>SS : ";
        displayInfoUserRankSS($username,$mode);
        echo "</br>S+ : ";
        displayInfoUserRankSH($username,$mode);
        echo "</br>S : ";
        displayInfoUserRankS($username,$mode);
        echo "</br>A : ";
        displayInfoUserRankA($username,$mode);

        displayTopPLays($username,$mode);
        displayRecentPLays($username,$mode);
        
    }
    else{
        echo "<form action='#' method='POST'>";
        echo "username : ";
        echo "<input type='text' name='username'></br>";
        echo "mode : ";
        echo "<select name='mode'></br>";
        echo "<option value='0'>osu! standart</option>";
        echo "<option value='1'>Taiko</option>";
        echo "<option value='2'>Catch The Beat</option>";
        echo "<option value='3'>osu! mania</option>";
        echo "</select></br>";
        echo "<input type='submit' value='rechercher'></br>";
        echo "</form>";
        echo "<p>dernieres beatmaps ranked ou loved</p>";
        echo "maps sorties aujourd'hui : </br>";
        DisplayLastBeatmaps(date("Y-m-d 0".(date('h')-2)));
        echo "maps sorties hier : </br>";
        DisplayLastBeatmaps(date("Y-m-".(date("d")-1)." 0".(date('h')-2)));
    }
    ?>
</body>
</html>