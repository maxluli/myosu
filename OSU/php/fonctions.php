<?php
    ///page de fonction php
    define("KEY","68ae0b4bb46af407940fcb62f732b2f14b39afc6"); 

    ///appels a l'API
    function GetLastBeatmaps($date){
        $result = file_get_contents ("http://osu.ppy.sh/api/get_beatmaps?k=".KEY."&limit=1000&since=".$date.":00:00");
        $maps = json_decode($result, true);
        return $maps;
    }
    function GetUser($username,$mode){
        //on verifie que on as pas deja appeler 'api pour venir recupèrer les data d'un user
        if(!isset($_SESSION["$username"])){
            $user = file_get_contents ('http://osu.ppy.sh/api/get_user?k='.KEY.'&u='.$username.'&m='.$mode);
            $result = json_decode($user, true);
            $_SESSION["$username"] = $result;
        }
        return($_SESSION["$username"]);
    }
    function getTopPlay($username,$mode){
        if(!isset($_SESSION["topPlays".$username])){
            $_SESSION["topPlays".$username] = json_decode(file_get_contents("http://osu.ppy.sh/api/get_user_best?k=68ae0b4bb46af407940fcb62f732b2f14b39afc6&u=".$username."&limit=100&m=".$mode),true);
        }
        //echo "<h1>".var_dump($_SESSION["topPlays".$username])."</h1>";
        return($_SESSION["topPlays".$username]);
    }
    function getMapInfo($beatmapId){
        if(file_exists("Cache/".$beatmapId.".txt")){
            //si elle existe alors on vas directement aller chercher les data dedans
            $mapJS = file_get_contents("Cache/".$beatmapId.".txt");
            $map = json_decode($mapJS,true);
        }
        else{
            //si elle n'existe pas encore, alors il faut la chercher et la stocker
            $mapJS = file_get_contents("http://osu.ppy.sh/api/get_beatmaps?k=68ae0b4bb46af407940fcb62f732b2f14b39afc6&b=".$beatmapId);
            file_put_contents("Cache/".$beatmapId.".txt",$mapJS);
            $map = json_decode($mapJS,true);
        }
        return($map);
    }
    function getRecentPlay($username,$mode){
        if(!isset($_SESSION["RecentPlays".$username])){
            $_SESSION["RecentPlays".$username] = json_decode(file_get_contents("http://osu.ppy.sh/api/get_user_recent?k=68ae0b4bb46af407940fcb62f732b2f14b39afc6&u=".$username."&limit=100&m=".$mode),true);
        }
        //echo "<h1>".var_dump($_SESSION["topPlays".$username])."</h1>";
        return($_SESSION["RecentPlays".$username]);
    }
    ///affichage
    function displayLastBeatmaps($date){
        $maps = GetLastBeatmaps($date);
        $before = "";
        foreach($maps as $a){
            if($a["beatmapset_id"] == $before){
                //on affiche rien
            }
            else{
                echo $a["title"]."</br>";
                echo "<img src='http://assets.ppy.sh/beatmaps/".$a["beatmapset_id"]."/covers/cover.jpg'></br>";
                $before = $a["beatmapset_id"];
            }
            
        }
    }
    function displayRecentPLays($username,$mode){
        $recent = getRecentPLay($username,$mode);
        $user = GetUser($username,$mode);

        foreach($recent as $b){
            $mapos = getMapInfo($b["beatmap_id"]);  
            echo "<a href='beatmap.php?idM=".$b["beatmap_id"]."&idU=".$user[0]["user_id"]."'>";
            echo "<img src='http://assets.ppy.sh/beatmaps/".$mapos[0]["beatmapset_id"]."/covers/cover.jpg'>";
            echo "</a></br>";
            echo $b["rank"]."</br>";
        }
    }
    function displayTopPLays($username,$mode){
        $top = getTopPlay($username,$mode);
        $user = GetUser($username,$mode);
        foreach($top as $a){    
            $map = getMapInfo($a["beatmap_id"]);       
            echo "</br><a href='beatmap.php?idM=".$a["beatmap_id"]."&idU=".$user[0]["user_id"]."'>";
            echo "<img src='http://assets.ppy.sh/beatmaps/".$map[0]["beatmapset_id"]."/covers/cover.jpg'>";
            echo "</a>";
            echo $a["pp"]."</br>";
        }
    }
    function displayPP($username,$mode){
        
        $user = getUser($username,$mode);
        echo "<img src='http://s.ppy.sh/a/".$user[0]["user_id"]."'><br/>";
    }
    function displayInfoUserRank($username,$mode){
        $user = getUser($username,$mode);
        echo $user[0]["pp_rank"];
    }
    function displayInfoUserJoinDate($username,$mode){
        $user = getUser($username,$mode);
        echo substr($user[0]["join_date"], 0, -9);
    }
    function displayInfoUserPLayTimeHours($user,$mode){
        $user = GetUser($user,$mode);
        $h = ConvertSecondsToHours($user[0]["total_seconds_played"]);
        echo $h;
    }
    function displayInfoUserPLayTimeDays($user,$mode){
        $user = GetUser($user,$mode);
        $j = ConvertSecondsToDays($user[0]["total_seconds_played"]);
        echo $j;
    }
    function displayInfoUserPlayCount($user,$mode){
        $user = GetUser($user,$mode);
        $count = $user[0]["playcount"];
        echo $count;
    }
    function displayInfoUserLevel($user,$mode){
        $user = GetUser($user,$mode);
        $pos = strpos($user[0]["level"],".",0);
        echo substr($user[0]["level"],0,$pos);
    }
    function displayInfoUserPepos($user,$mode){
        $user = GetUser($user,$mode);
        $pos = strpos($user[0]["pp_raw"],".",0);
        echo substr($user[0]["pp_raw"],0,$pos+3);
    }
    function displayInfoUserAccu($user,$mode){
        $user = GetUser($user,$mode);
        $pos = (strpos($user[0]["accuracy"],".",0))+4;
        echo substr($user[0]["accuracy"],0,$pos)."%";
    }
    function displayInfoUserPays($user,$mode){
        $user = GetUser($user,$mode);
        echo $user[0]["country"];
    }
    function displayInfoUserCountryRank($user,$mode){
        $user = GetUser($user,$mode);
        echo $user[0]["pp_country_rank"];
    }
    function displayInfoUserRankSSH($user,$mode){
        $user = GetUser($user,$mode);
        echo $user[0]["count_rank_ssh"];
    }
    function displayInfoUserRankSS($user,$mode){
        $user = GetUser($user,$mode);
        echo $user[0]["count_rank_ss"];
    }
    function displayInfoUserRankSH($user,$mode){
        $user = GetUser($user,$mode);
        echo $user[0]["count_rank_sh"];
    }
    function displayInfoUserRankS($user,$mode){
        $user = GetUser($user,$mode);
        echo $user[0]["count_rank_s"];
    }
    function displayInfoUserRankA($user,$mode){
        $user = GetUser($user,$mode);
        echo $user[0]["count_rank_a"];
    }
    ///calculs
    function ConvertSecondsToHours($seconds){
        $minuts = ($seconds/60);
        $hours = ($minuts/60);
        $pos = strpos($hours,".",0);
        $heures = substr($hours,0,$pos);
        return $heures;
    }
    function ConvertSecondsToDays($seconds){
        $minuts = ($seconds/60);
        $hours = ($minuts/60);
        $days = ($hours/24);
        $pos = strpos($days,".",0);
        $jours = substr($days,0,$pos);
        return $jours;
    }
    function computeAccuracy($count50, $count100, $count300, $countMiss) {
        return ((50 * $count50) + (100 * $count100) + (300 * $count300)) / (300 * ($countMiss + $count50 + $count100 + $count300));
    }
?>